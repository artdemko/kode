from django.conf import settings

API_KEY = getattr(settings, "OPENWEATHERMAP_API_KEY", None)
API_UNITS = getattr(settings, "OPENWEATHERMAP_API_UNITS", None)
API_V = getattr(settings, "OPENWEATHERMAP_API_V", None)

API_URL = getattr(settings, "OPENWEATHERMAP_API_URL", None)
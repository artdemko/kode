from django.db import models
import datetime

class City(models.Model):
  name = models.CharField(max_length=155)
  country = models.CharField(max_length=2)

  def get_weather(self):
    now = datetime.datetime.now()
    earlier = now - datetime.timedelta(hours=1)
    return self.weather.filter(created__range=(earlier,now))

  def __str__(self):
    return self.name

class Weather(models.Model):
  city = models.ForeignKey(City, related_name="weather")
  wind_speed = models.FloatField(null=True, blank=True)
  temp = models.FloatField(null=True, blank=True)
  pressure = models.FloatField(null=True, blank=True)

  created = models.DateTimeField(auto_now_add=True)
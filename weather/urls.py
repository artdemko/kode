from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
urlpatterns = router.urls

urlpatterns += [
  url(r'^/$', CityWeather.as_view()),
]
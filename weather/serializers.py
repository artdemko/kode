from rest_framework import serializers
from .models import Weather, City

class CitySerializer(serializers.ModelSerializer):
  class Meta:
    model = City
    fields = [
      'name',
    ]

class WeatherSerializer(serializers.ModelSerializer):
  
  city = CitySerializer(many=False, read_only=True)

  class Meta:
    model = Weather
    fields = [
      'temp',
      'pressure',
      'wind_speed',
      'city',
    ]
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import status
from .openweather import OpenWeather
from rest_framework.response import Response
from .models import City, Weather
from .serializers import WeatherSerializer

class CityWeather(APIView):

  def get(self, request, format=None):

    try:
      request_city = request.GET['city']
    except:
      return Response({"status":"error"}, status=status.HTTP_400_BAD_REQUEST)

    city, created = City.objects.get_or_create(name=request_city)

    try:
      city_weather = city.get_weather()[0]
    except:
      open_weather = OpenWeather()
      weather = open_weather.weather(request_city)

      if weather['cod']!=200:
        return Response(weather, status=status.HTTP_400_BAD_REQUEST)

      new_weather = Weather()
      new_weather.city = city
      new_weather.wind_speed = weather['wind']['speed']
      new_weather.temp = weather['main']['temp']
      new_weather.pressure = weather['main']['pressure']
      new_weather.save()
      city_weather = new_weather

    serializer = WeatherSerializer(city_weather)

    return Response(serializer.data, status=status.HTTP_200_OK)

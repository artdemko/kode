from .config import *
import requests
import json

class OpenWeather(object):
  def __init__(self):
    self.API_KEY = API_KEY
    self.API_UNITS = API_UNITS
    self.API_V = API_V
    self.API_URL = API_URL

  def weather(self, city):
    #api.openweathermap.org/data/2.5/weather?q=London
    params = {}
    params['q'] = city
    params['appid'] = self.API_KEY
    if self.API_UNITS:
      params['units'] = self.API_UNITS
    url = "%s/data/%s/weather?%s" % (self.API_URL, self.API_V, self.get_uri_params(params),)
    response = requests.get(url)
    return json.loads(response.text)

  def get_uri_params(self, params):
    result = ""
    for param, value in params.items():
      result += "%s=%s&" % (param, value,)  
    return result[:-1]